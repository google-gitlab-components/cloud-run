# deploy-cloud-run

**Status: [Beta][beta]**

> - `google_cloud_support_feature_flag` (Beta) flag needs to be enabled to use the Component.

The `deploy-cloud-run` GitLab Component automates the deployment of your Cloud Run services within your GitLab CI/CD pipeline. The component offers flexible deployment behavior,
creating a brand-new service if one doesn't already exist in your project and region. Conversely, if a Cloud Run service with the same name is already present, the component efficiently updates it to a new revision using your inputs.

[[_TOC_]]

## Prerequisites

- Set up Google Cloud workload identity federation with [GitLab - Google Cloud integration onboarding process][onboarding].
- [Configure](#authorization) the permissions for the workload identity you use to deploy Cloud Run services.
- If deploying from source, [create][ar-create] an Artifact Registry repository named `cloud-run-source-deploy` in desired region. Note that running `gcloud run deploy --source .` locally in the source directory will create it as part of the deployment.

## Billing

Usage of the `deploy-cloud-run` GitLab component might incur Google Cloud billing charges depending on your usage. For more information on Cloud Run pricing, see
[Cloud Run Pricing][pricing].

## Usage

Add the following to your `gitlab-ci.yml`:
``` yaml
include:
  - component: gitlab.com/google-gitlab-components/cloud-run/deploy-cloud-run@<VERSION>
    inputs:
      project_id: "my-project"
      service: "service-name"
      region: "us-central1"
      image: "us-docker.pkg.dev/cloudrun/container/hello:latest"
      source: "app"
      stage: "my-stage"
      as: "my-job-name"
```

Note that `image` and `source` are mutually exclusive.
If neither is specified, default to deploying source from the repository root.

## Authentication

- The component authenticates to Google Cloud services using workload identity federation. To create a workload identity pool and provider for this integration, follow the steps in the GitLab tutorial [Google Cloud workload identity federation and IAM policies][onboarding].

## Inputs

| Input                        | Description                                                                  | Example           | Default Value      |
|------------------------------|------------------------------------------------------------------------------|-------------------|--------------------|
| `project_id`                 | (Required) The ID of the Google Cloud project in which to deploy the service | `my-project`      |                    |
| `region`                     | (Required) Region in which to deploy the service                             | `us-central1`     |                    |
| `service`                    | (Required) ID of the service or fully-qualified identifier of the service    | `my-service-name` |                    |
| `image`                      | (Optional) Fully-qualified name of the container image to deploy             | `us-docker.pkg.dev/cloudrun/container/hello`                 |                    |
| `source`                     | (Optional) Relative path from the repository root for deploying from source  | `app`             | "." if `image` not set                   |
| `stage`                      | (Optional) The GitLab CI/CD stage                                            | `my-stage`        | `deploy`           |
| `as`                         | (Optional) The name of the job to execute                                    | `my-job-name`     | `deploy-cloud-run` |


## Authorization

To ensure smooth and secure interaction with Google Cloud services, it's essential to configure appropriate authorization settings.

### Deploy Cloud Run Component Operation Permissions

To use the `deploy-cloud-run` component, grant the following roles to your workload identity pool:
- Cloud Storage Admin ([`roles/run.admin`][sa-run-admin]) to get, create and update a service.
- Service Account User ([`roles/iam.serviceAccountUser`][sa-sa-user]) to run operations as the service account.
- Cloud Build Editor ([`roles/cloudbuild.builds.editor`][sa-build-editor]): to build the service.

To grant the Cloud Run Admin, IAM Service Account User, and Cloud Build Editor roles to all principals matching `developer_access=true` attribute mapping in your workload identity pool, run the following command:

``` bash
# Replace ${PROJECT_ID}, ${PROJECT_NUMBER}, ${LOCATION}, ${POOL_ID} with your values below

WORKLOAD_IDENTITY=principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/attribute.developer_access/true

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/run.admin"
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/iam.serviceAccountUser"
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/cloudbuild.builds.editor"
```

### Cloud Run Runtime Service Account

By default, Cloud Run uses the [default Compute Engine service account][default-sa] (`[project-number]-compute@developer.gserviceaccount.com`) as the internal [execution service account][execution-sa] to deploy your service. That service account has sufficient permissions by default.

This Component doesn't support setting up other service accounts for cloud run services.


[beta]: https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html
[default-sa]: https://cloud.google.com/iam/docs/service-account-types#default
[execution-sa]: https://cloud.google.com/deploy/docs/cloud-deploy-service-account#execution_service_account
[sa-sa-user]: https://cloud.google.com/iam/docs/understanding-roles#iam.serviceAccountUser
[sa-run-admin]: https://cloud.google.com/run/docs/reference/iam/roles#run.admin
[sa-build-editor]: https://cloud.google.com/build/docs/iam-roles-permissions#predefined_roles
[google-project]: https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects
[region]: https://cloud.google.com/compute/docs/regions-zones
[pricing]: https://cloud.google.com/run/pricing
[ar-create]: https://cloud.google.com/artifact-registry/docs/repositories/create-repos